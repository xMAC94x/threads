use core::future::Future;

pub trait ThreadPoolSyncConsumer {
    fn execute_sync<F>(&self, f: F)
    where
        F: FnOnce() + Send + 'static;
}

pub trait ThreadPoolAsyncConsumer {
    fn execute_async<F>(&mut self, f: F) -> <F as Future>::Output
    where
        F: Future + Send + 'static,
        F::Output: Send + 'static;
}

pub trait ThreadPoolAsyncEndlessConsumer {
    fn execute_async_endless<F>(&mut self, f: F)
    where
        F: Future<Output = ()> + Send + 'static;
}

impl<AC: ThreadPoolAsyncConsumer> ThreadPoolAsyncEndlessConsumer for AC {
    fn execute_async_endless<F>(&mut self, f: F) -> <F as Future>::Output
    where
        F: Future<Output = ()> + Send + 'static,
    {
        self.execute_async(f)
    }
}

pub trait ThreadPoolRuntimeStatistics {
    fn active_jobs(&self) -> Option<usize>;
    fn queued_jobs(&self) -> Option<usize>;
    fn completed_jobs(&self) -> Option<usize>;
}
