use crate::pool::{ThreadPoolAsyncConsumer, ThreadPoolAsyncEndlessConsumer, ThreadPoolSyncConsumer};
use core::fmt::Debug;

pub trait BuilderMax {
    fn set_max_threads(self, num: usize) -> Self;
}

pub trait BuilderMin {
    fn set_min_threads(self, num: usize) -> Self;
}

pub trait BuilderNamePrefix {
    fn set_name_prefix<S: Into<String>>(self, s: S) -> Self;
}

pub trait BuilderStackSize {
    fn set_stack_size(self, num: usize) -> Self;
}

pub trait ThreadPoolSyncBuilder {
    type Builder: ThreadPoolSyncConsumer;
    type Error: Debug;

    fn build(self) -> Result<Self::Builder, Self::Error>;
}

pub trait ThreadPoolAsyncBuilder {
    type Builder: ThreadPoolAsyncConsumer;
    type Error: Debug;

    fn build(self) -> Result<Self::Builder, Self::Error>;
}

pub trait ThreadPoolAsyncEndlessBuilder {
    type Builder: ThreadPoolAsyncEndlessConsumer;
    type Error: Debug;

    fn build(self) -> Result<Self::Builder, Self::Error>;
}

pub trait ThreadPoolFullBuilder {
    type Builder: ThreadPoolSyncConsumer + ThreadPoolAsyncConsumer;
    type Error: Debug;

    fn build(self) -> Result<Self::Builder, Self::Error>;
}
