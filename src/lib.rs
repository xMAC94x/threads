// Support using Threads without the standard library!
//#![cfg_attr(not(feature = "std"), no_std)]

mod builder;
#[cfg(any(feature = "derive_uvth", feature = "derive_tokio"))]
mod derive;
mod pool;

pub use builder::{
    BuilderMax, BuilderMin, BuilderNamePrefix, ThreadPoolAsyncBuilder, ThreadPoolAsyncEndlessBuilder,
    ThreadPoolFullBuilder, ThreadPoolSyncBuilder,
};
pub use pool::{
    ThreadPoolAsyncConsumer, ThreadPoolAsyncEndlessConsumer, ThreadPoolRuntimeStatistics, ThreadPoolSyncConsumer,
};
