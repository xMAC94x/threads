mod helper;

#[test]
fn threadpool_pool() {
    let pool = threadpool::Builder::new().build();
    helper::verify_count_threadpool(&pool);
}

#[test]
fn threadpool_threads() {
    let pool = threadpool::Builder::new().num_threads(5).build();
    let x = helper::verify_cores_threadpool(&pool, 5);
    assert!(x > 0.95);
    assert!(x < 1.05);
}

#[test]
fn threadpool_build() {
    let builder = threadpool::Builder::new();
    let pool = helper::build_pool_with_cores(builder, 3);
    let x = helper::verify_cores_threadpool(&pool, 3);
    assert!(x > 0.95);
    assert!(x < 1.05);
}
