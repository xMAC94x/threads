mod helper;

#[test]
fn uvth_pool() {
    let pool = uvth::ThreadPoolBuilder::new().build();
    helper::verify_count_threadpool(&pool);
}

#[test]
fn verify_threads() {
    let pool = uvth::ThreadPoolBuilder::new().num_threads(5).build();
    let x = helper::verify_cores_threadpool(&pool, 5);
    assert!(x > 0.95);
    assert!(x < 1.05);
}

#[test]
fn uvth_build() {
    let builder = uvth::ThreadPoolBuilder::new();
    let pool = helper::build_pool_with_cores(builder, 3);
    let x = helper::verify_cores_threadpool(&pool, 3);
    assert!(x > 0.95);
    assert!(x < 1.05);
}
