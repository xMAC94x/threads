use crate::pool::*;

// Builder

// ThreadPool

impl ThreadPoolSyncConsumer for scheduled_thread_pool::ScheduledThreadPool {
    fn execute_sync<F>(&self, f: F)
    where
        F: FnOnce() + Send + 'static,
    {
        self.execute(f);
    }
}
