# Threads

This crate contains traits to abstract the functionality from various threadpools.
This crate serves 2 purposes

### users of Threadpools
You are developing a `lib` that needs some sort of threadpool, but you don't want to force  a specific one?
Especially since every crate seems to have another threadpool and managing those is a difficult task.

Base your crate on `ThreadPoolSyncConsumer` or `ThreadPoolASyncConsumer`.
Consumers of your crate can then choose which implementation they want.

### creators of Threadpools
You are working on a new threadpool crate? But it's hard to fit in this ecosystem as there are already many pools.
This crate is build by analysing many threadpool crates and while your implementation might not fit 100% to our traits.
Implementing the `ThreadPoolSyncConsumer` or `ThreadPoolASyncConsumer` traits might make your crate more attractive for others.

In case you support a builder-pattern there are additional traits to implement, e.g. for setting a `thread_max`

## Motivation:
https://xkcd.com/927/

There are many threadpools deply integrated in rust crates, but not a single `trait` to cover all

- [ ] `tokio` https://docs.rs/tokio/0.2.22/tokio/runtime/struct.Runtime.html#method.new
- [ ] `futures` https://rust-lang-nursery.github.io/futures-api-docs/0.3.0-alpha.4/futures/executor/struct.ThreadPool.html
- [ ] `rayon` https://doc.servo.org/rayon/struct.ThreadPool.html
- [ ] `threadpool` https://docs.rs/threadpool/1.8.1/threadpool/struct.ThreadPool.html
- (out of scope) `scoped_threadpool` http://kimundi.github.io/scoped-threadpool-rs/scoped_threadpool/struct.Pool.html
- [ ] `scheduled_thread_pool` https://docs.rs/scheduled-thread-pool/0.2.5/scheduled_thread_pool/struct.ScheduledThreadPool.html
- [ ] `tiny-http` https://github.com/tiny-http/tiny-http/blob/master/src/util/task_pool.rs
- [ ] `tokio_io_pool` https://docs.rs/tokio-io-pool/0.1.6/tokio_io_pool/struct.Runtime.html
- [ ] `uvth` https://docs.rs/uvth/4.0.1/uvth/struct.ThreadPool.html

there is one trait that tries to archive something similar but adds alot of dependencies:
- `futures-cputask` https://docs.rs/futures-cputask/0.3.0/futures_cputask/trait.SyncThreadPool.html

## License

Licensed under either of

 * Apache License, Version 2.0
   ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
 * MIT license
   ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.

## Contribution

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in the work by you, as defined in the Apache-2.0 license, shall be
dual licensed as above, without any additional terms or conditions.


## Small story about which threadpool to choose.
1. Do you need large

## differencies between all threadpools
- scoped vs unscoped?
- async vs sync
- endless vs jobs with a return
- result vs panic
