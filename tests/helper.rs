use std::sync::{
    atomic::{AtomicU64, Ordering},
    Arc,
};
use threads::{BuilderMax, ThreadPoolSyncBuilder, ThreadPoolSyncConsumer};

pub fn verify_count_threadpool<P: ThreadPoolSyncConsumer>(pool: &P) {
    let counter = Arc::new(AtomicU64::new(0));
    for _ in 0..5 {
        let counter = counter.clone();
        pool.execute_sync(move || {
            counter.fetch_add(1, Ordering::Relaxed);
        })
    }
    std::thread::sleep(std::time::Duration::from_millis(500));
    assert_eq!(counter.load(Ordering::Relaxed), 5);
}

pub fn verify_cores_threadpool<P: ThreadPoolSyncConsumer>(pool: &P, assumed: u64) -> f64 {
    let counter = Arc::new(AtomicU64::new(0));
    const ROUNDS: u32 = 5;
    const SLEEP_MILLIS: u64 = 1000;
    let tries = assumed * ROUNDS as u64;
    let now = std::time::Instant::now();
    for _ in 0..tries {
        let counter = counter.clone();
        pool.execute_sync(move || {
            std::thread::sleep(std::time::Duration::from_millis(SLEEP_MILLIS));
            counter.fetch_add(1, Ordering::Relaxed);
        })
    }
    while counter.load(Ordering::Relaxed) != tries {
        std::thread::sleep(std::time::Duration::from_millis(10));
    }
    let duration = now.elapsed();
    duration.as_millis() as f64 / ((SLEEP_MILLIS * ROUNDS as u64) as f64)
}

pub fn build_pool_with_cores<B: ThreadPoolSyncBuilder + BuilderMax>(builder: B, cores: usize) -> B::Builder {
    builder.set_max_threads(cores).build().unwrap()
}
