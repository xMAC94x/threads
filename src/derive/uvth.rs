use crate::{builder::*, pool::*};

// Builder

impl BuilderMax for uvth::ThreadPoolBuilder {
    fn set_max_threads(self, num: usize) -> Self { self.num_threads(num) }
}

impl BuilderNamePrefix for uvth::ThreadPoolBuilder {
    fn set_name_prefix<S: Into<String>>(self, s: S) -> Self { self.name(s.into()) }
}

impl BuilderStackSize for uvth::ThreadPoolBuilder {
    fn set_stack_size(self, num: usize) -> Self { self.stack_size(num) }
}

impl ThreadPoolSyncBuilder for uvth::ThreadPoolBuilder {
    type Builder = uvth::ThreadPool;
    type Error = ();

    fn build(self) -> Result<Self::Builder, Self::Error> { Ok(self.build()) }
}

// ThreadPool

impl ThreadPoolSyncConsumer for uvth::ThreadPool {
    fn execute_sync<F>(&self, f: F)
    where
        F: FnOnce() + Send + 'static,
    {
        self.execute(f)
    }
}

/*
#[cfg(feature = "derive_uvth")]
impl ThreadPoolBuilderMax for uvth::ThreadPool {
    fn set_max_threads(self, num: usize) -> Self {
        self.set_num_threads(num);
        self
    }
}
*/
