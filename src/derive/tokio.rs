use crate::{builder::*, pool::*};
use core::future::Future;

// Builder

impl BuilderMin for tokio::runtime::Builder {
    fn set_min_threads(mut self, num: usize) -> Self {
        self.worker_threads(num);
        self
    }
}

impl BuilderMax for tokio::runtime::Builder {
    fn set_max_threads(mut self, num: usize) -> Self {
        self.worker_threads(num);
        self
    }
}

impl BuilderNamePrefix for tokio::runtime::Builder {
    fn set_name_prefix<S: Into<String>>(mut self, s: S) -> Self {
        self.thread_name(s);
        self
    }
}

impl BuilderStackSize for tokio::runtime::Builder {
    fn set_stack_size(mut self, num: usize) -> Self {
        self.thread_stack_size(num);
        self
    }
}

impl ThreadPoolAsyncEndlessBuilder for tokio::runtime::Builder {
    type Builder = tokio::runtime::Runtime;
    type Error = std::io::Error;

    fn build(mut self) -> Result<Self::Builder, Self::Error> { <Self>::build(&mut self) }
}

// ThreadPool

/*
impl ThreadPoolAsyncConsumer for tokio::runtime::Runtime {
    fn execute_async<F>(&mut self, f: F) -> <F as Future>::Output
        where
            F: Future + Send + 'static,
            F::Output: Send + 'static {
        self.spawn(f).await.expect("this is a temporary unwrap thats needed till we have a unique interface")
    }
}
 */

impl ThreadPoolAsyncEndlessConsumer for tokio::runtime::Runtime {
    fn execute_async_endless<F>(&mut self, f: F)
    where
        F: Future<Output = ()> + Send + 'static,
    {
        self.spawn(f);
    }
}
