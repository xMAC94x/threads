use crate::{builder::*, pool::*};

// Builder

impl BuilderMax for rayon::ThreadPoolBuilder {
    fn set_max_threads(self, num: usize) -> Self { self.num_threads(num) }
}

/*
#[cfg(feature = "derive_rayon")]
impl ThreadPoolBuilderNamePrefix for rayon::ThreadPoolBuilder {
    fn set_name_prefix<S: Into<String>>(self, s: S) -> Self {
        self.thread_name(|_| s.into())
    }
}
 */

impl BuilderStackSize for rayon::ThreadPoolBuilder {
    fn set_stack_size(self, num: usize) -> Self { self.stack_size(num) }
}

impl ThreadPoolSyncBuilder for rayon::ThreadPoolBuilder {
    type Builder = rayon::ThreadPool;
    type Error = rayon::ThreadPoolBuildError;

    fn build(self) -> Result<Self::Builder, Self::Error> { self.build() }
}

// ThreadPool

impl ThreadPoolSyncConsumer for rayon::ThreadPool {
    fn execute_sync<F>(&self, f: F)
    where
        F: FnOnce() + Send + 'static,
    {
        self.spawn(f)
    }
}
