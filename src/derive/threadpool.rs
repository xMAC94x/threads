use crate::{builder::*, pool::*};

// Builder

impl BuilderMax for threadpool::Builder {
    fn set_max_threads(self, num: usize) -> Self { self.num_threads(num) }
}

impl BuilderNamePrefix for threadpool::Builder {
    fn set_name_prefix<S: Into<String>>(self, s: S) -> Self { self.thread_name(s.into()) }
}

impl BuilderStackSize for threadpool::Builder {
    fn set_stack_size(self, num: usize) -> Self { self.thread_stack_size(num) }
}

impl ThreadPoolSyncBuilder for threadpool::Builder {
    type Builder = threadpool::ThreadPool;
    type Error = ();

    fn build(self) -> Result<Self::Builder, Self::Error> { Ok(self.build()) }
}

// ThreadPool

impl ThreadPoolSyncConsumer for threadpool::ThreadPool {
    fn execute_sync<F>(&self, f: F)
    where
        F: FnOnce() + Send + 'static,
    {
        self.execute(f)
    }
}
