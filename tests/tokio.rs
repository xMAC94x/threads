mod helper_async;

use tokio::runtime;

#[test]
fn tokio_pool() {
    let mut pool = runtime::Builder::new_multi_thread().build().unwrap();
    helper_async::verify_count_threadpool_async(&mut pool);
}

#[test]
fn tokio_threads() {
    let mut pool = tokio::runtime::Builder::new_multi_thread()
        .worker_threads(5)
        .enable_time() // needed for time tests
        .build()
        .unwrap();
    let x = helper_async::verify_cores_threadpool_async(&mut pool, 5);
    assert!(x > 0.95);
    assert!(x < 1.05);
}

#[test]
fn tokio_build() {
    let mut builder = tokio::runtime::Builder::new_multi_thread();
    builder.enable_time(); // needed for time tests
    let mut pool = helper_async::build_pool_with_cores_async(builder, 3);
    let x = helper_async::verify_cores_threadpool_async(&mut pool, 3);
    assert!(x > 0.95);
    assert!(x < 1.05);
}
