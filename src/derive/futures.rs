use crate::{builder::*, pool::*};
use core::future::Future;

// Builder

impl BuilderMax for futures_executor::ThreadPoolBuilder {
    fn set_max_threads(mut self, num: usize) -> Self {
        self.pool_size(num);
        self
    }
}

impl BuilderNamePrefix for futures_executor::ThreadPoolBuilder {
    fn set_name_prefix<S: Into<String>>(mut self, s: S) -> Self {
        self.name_prefix(s.into());
        self
    }
}

impl BuilderStackSize for futures_executor::ThreadPoolBuilder {
    fn set_stack_size(mut self, num: usize) -> Self {
        self.stack_size(num);
        self
    }
}

impl ThreadPoolAsyncEndlessBuilder for futures_executor::ThreadPoolBuilder {
    type Builder = futures_executor::ThreadPool;
    type Error = std::io::Error;

    fn build(mut self) -> Result<Self::Builder, Self::Error> { self.create() }
}

// ThreadPool

impl ThreadPoolAsyncEndlessConsumer for futures_executor::ThreadPool {
    fn execute_async_endless<F>(&mut self, f: F)
    where
        F: Future<Output = ()> + Send + 'static,
    {
        self.spawn_ok(f)
    }
}
